'use strict'
/*
Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

Використання try...catch для перехоплення помилок під час виконання коду.
Це дозволяє вам обробляти помилки та продовжувати виконання коду,
Наприклад, ви можете використовувати try...catch для обробки помилок вводу-виводу або помилок JSON.

Локалізація проблем:

Використання try...catch для локалізації проблем у коді.
Блок catch може містити код для запису журналу помилок або відображення повідомлення користувачеві.
3. Забезпечення стійкості до помилок:

Використання try...catch для створення коду, стійкого до помилок.
Це може допомогти запобігти аварійному завершенню роботи програми, якщо виникає помилка.



Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

*/

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const bookContainer = document.createElement('div');
bookContainer.setAttribute('id', 'root');
const bookList = document.createElement('ul');

document.body.append(bookContainer);
bookContainer.append(bookList);


class BooksAbsentParameterError extends Error {
    constructor() {

        super('Book with absent elements');
        this.name = 'BooksAbsentParameterError';
    };
};


books.forEach(elem => {
    try {
        if (elem.author !== undefined && elem.name !== undefined && elem.price !== undefined) {
            bookList.insertAdjacentHTML('beforeend', `
            <li>Автор: ${elem.author}, Назва: ${elem.name}, Ціна: ${elem.price}грн</li>
            `);


        } else {
            throw new BooksAbsentParameterError();
        }

    } catch (err) {
        if (err.name === 'BooksAbsentParameterError') {
            console.warn(`${err}: ${elem.author}, ${elem.name}, ${elem.price}`)                  
        }
        else {
            throw err;
        }
    }
});







